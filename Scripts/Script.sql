CREATE TABLE email (id int not null AUTO_increment primary key, vorname varchar(30), nachname varchar(30), email varchar(50));

insert into email (vorname, nachname, email) values('Bastian', 'Krueger', 'bastian@krueger.de');
insert into email (vorname, nachname, email) values('Lucas', 'Theise', 'lucas@dumm.de');
insert into email (vorname, nachname, email) values('Lucas', 'Knoppers', 'lukas@knoppers.de');
insert into email (vorname, nachname, email) values('Nintendo', 'Spast', 'spasten@nintendo.de');

select * from email;