package me.unbk.Emailverwaltung;

import java.sql.ResultSet;

public class DBUtils {
	
	public static void close(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {
            System.err.println("[SQL] Failed to close ResultSet:");
            e.printStackTrace();
        }
    }

}
