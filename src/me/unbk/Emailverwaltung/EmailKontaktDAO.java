package me.unbk.Emailverwaltung;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.w3c.dom.html.HTMLScriptElement;

public class EmailKontaktDAO {

	private String host = "localhost";
	private int port = 3306;
	private String user = "root";
	private String password = "";
	private String database = "rp";
	private Connection conn;

	public EmailKontaktDAO() throws ClassNotFoundException {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://" + user + ":" + password + "@" + host + ":" + port + "/" + database + "?serverTimezone=Europe/Berlin");

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

	public EmailKontakt create(EmailKontakt eK) {

		String sql = "INSERT INTO email (vorname, nachname, email) VALUES (\"" + eK.getVorname() + "\", \"" + eK.getNachname() + "\", \"" + eK.getEmail() + "\")";
		int insertedID = 0;

		PreparedStatement pS = null;
		ResultSet rsID = null;

		try {

			pS = this.conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			pS.executeUpdate();

			rsID = pS.getGeneratedKeys();
			rsID.next();
			insertedID = rsID.getInt(1);

		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			DBUtils.close(rsID);

		}
		
		return this.select(insertedID);
	}

	public void delete(EmailKontakt sEK) {
		
		String sql = "DELETE FROM email WHERE ID =" + sEK.getId() + "";
		PreparedStatement pS = null;
		
		try {

			pS = this.conn.prepareStatement(sql);

			pS.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();
		}

		System.out.println("entry deleted");
		
	}

	public void insert(EmailKontakt eK) {
		return;
	}

	public void update(EmailKontakt sEK) {
		return;
	}

	public EmailKontakt next(EmailKontakt pEK) {
		
		String sql = "SELECT * FROM email WHERE id > " + pEK.getId() + " ORDER BY id ASC LIMIT 1";

		PreparedStatement pS = null;
		ResultSet rS = null;
		EmailKontakt eK = new EmailKontakt(-1, "", "", "");

		try {
			pS = this.conn.prepareStatement(sql);
			rS = pS.executeQuery();
			while (rS.next()) {
				eK.setId(rS.getInt("id"));
				eK.setVorname(rS.getString("vorname"));
				eK.setNachname(rS.getString("nachname"));
				eK.setEmail(rS.getString("email"));

				this.log(rS.getInt("id"));
			}

		} catch (SQLException e) {
			System.out.println("[SQL] Error: ");
			e.printStackTrace();

		} finally {
			DBUtils.close(rS);

		}

		return eK;
	}

	public EmailKontakt previous(EmailKontakt pEK) {

		String sql = "SELECT * FROM email WHERE id < " + pEK.getId() + " ORDER BY id DESC LIMIT 1";

		PreparedStatement pS = null;
		ResultSet rS = null;
		EmailKontakt eK = new EmailKontakt(-1, "", "", "");

		try {
			pS = this.conn.prepareStatement(sql);
			rS = pS.executeQuery();
			while (rS.next()) {
				eK.setId(rS.getInt("id"));
				eK.setVorname(rS.getString("vorname"));
				eK.setNachname(rS.getString("nachname"));
				eK.setEmail(rS.getString("email"));

				this.log(rS.getInt("id"));
			}

		} catch (SQLException e) {
			System.out.println("[SQL] Error: ");
			e.printStackTrace();

		} finally {
			DBUtils.close(rS);

		}

		return eK;
	}

	public EmailKontakt first() {

		String sql = "SELECT * from email ORDER BY id ASC LIMIT 1";

		PreparedStatement pS = null;
		ResultSet rS = null;
		EmailKontakt eK = new EmailKontakt(-1, "", "", "");

		try {
			pS = this.conn.prepareStatement(sql);
			rS = pS.executeQuery();
			while (rS.next()) {
				eK.setId(rS.getInt("id"));
				eK.setVorname(rS.getString("vorname"));
				eK.setNachname(rS.getString("nachname"));
				eK.setEmail(rS.getString("email"));

				this.log(rS.getInt("id"));
			}

		} catch (SQLException e) {
			System.out.println("[SQL] Error: ");
			e.printStackTrace();

		} finally {
			DBUtils.close(rS);

		}

		return eK;
	}

	public EmailKontakt last() {

		String sql = "SELECT * from email ORDER BY id DESC LIMIT 1";

		PreparedStatement pS = null;
		ResultSet rS = null;
		EmailKontakt eK = new EmailKontakt(-1, "", "", "");

		try {
			pS = this.conn.prepareStatement(sql);
			rS = pS.executeQuery();
			while (rS.next()) {
				eK.setId(rS.getInt("id"));
				eK.setVorname(rS.getString("vorname"));
				eK.setNachname(rS.getString("nachname"));
				eK.setEmail(rS.getString("email"));

				this.log(rS.getInt("id"));
			}


		} catch (SQLException e) {
			System.out.println("[SQL] Error: ");
			e.printStackTrace();

		} finally {
			DBUtils.close(rS);

		}

		return eK;
	}

	public EmailKontakt select(int id) {

		String sql = "SELECT * from email WHERE id = " + id;

		PreparedStatement pS = null;
		ResultSet rS = null;
		EmailKontakt eK = new EmailKontakt(-1, "", "", "");

		try {
			pS = this.conn.prepareStatement(sql);
			rS = pS.executeQuery();
			while (rS.next()) {
				eK.setId(rS.getInt("id"));
				eK.setVorname(rS.getString("vorname"));
				eK.setNachname(rS.getString("nachname"));
				eK.setEmail(rS.getString("email"));

				this.log(rS.getInt("id"));
			}

		} catch (SQLException e) {
			System.out.println("[SQL] Error: ");
			e.printStackTrace();

		} finally {
			DBUtils.close(rS);

		}

		return eK;
	}

	private void log(int id) {
		System.out.println("[SQL] fetched props from object id " + id);

	}

}
