package me.unbk.Emailverwaltung;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

public class JFrameEmailverwaltung extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5399064356901228963L;
	
	private JPanel contentPane;
	private JLabel lblId;
	private JLabel lblVorname;
	private JLabel lblNachname;
	private JLabel lblEmail;
	private JButton btnSearch;
	private JTextField txtId;
	private JTextField txtVorname;
	private JTextField txtNachname;
	private JTextField txtEmail;
	private JButton btnLast;
	private JButton btnFirst;
	private JButton btnBackwards;
	private JButton btnForward;
	private JButton btnDelete;
	private JButton btnSave;
	private JButton btnNew;
	
	private EmailKontaktDAO emailKontaktDAO;
	private EmailKontakt lastEK = new EmailKontakt(-1, "", "", "");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrameEmailverwaltung frame = new JFrameEmailverwaltung();
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

					SwingUtilities.updateComponentTreeUI(frame);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFrameEmailverwaltung() {
		
		try {
			this.emailKontaktDAO = new EmailKontaktDAO();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		initGUI();
	}
	private void initGUI() {
		setResizable(false);
		setTitle("WINDOW BUILDER IST GANZ GROSSER KOT!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 690, 192);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		{
			lblId = new JLabel("ID");
			lblId.setBounds(10, 11, 85, 14);
			contentPane.add(lblId);
		}
		{
			lblVorname = new JLabel("Vorname");
			lblVorname.setBounds(10, 43, 85, 14);
			contentPane.add(lblVorname);
		}
		{
			lblNachname = new JLabel("Nachname");
			lblNachname.setBounds(10, 74, 85, 14);
			contentPane.add(lblNachname);
		}
		{
			lblEmail = new JLabel("E-Mail");
			lblEmail.setBounds(10, 105, 85, 14);
			contentPane.add(lblEmail);
		}
		{
			btnSearch = new JButton("Suchen");
			btnSearch.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					do_btnSearch_actionPerformed(e);
				}
			});
			btnSearch.setBounds(585, 7, 89, 23);
			contentPane.add(btnSearch);
		}
		{
			txtId = new JTextField();
			txtId.setText("");
			txtId.setBounds(90, 8, 485, 20);
			contentPane.add(txtId);
			txtId.setColumns(10);
		}
		{
			txtVorname = new JTextField();
			txtVorname.setText("");
			txtVorname.setBounds(90, 40, 584, 20);
			contentPane.add(txtVorname);
			txtVorname.setColumns(10);
		}
		{
			txtNachname = new JTextField();
			txtNachname.setText("");
			txtNachname.setBounds(90, 71, 584, 20);
			contentPane.add(txtNachname);
			txtNachname.setColumns(10);
		}
		{
			txtEmail = new JTextField();
			txtEmail.setText("");
			txtEmail.setBounds(90, 102, 584, 20);
			contentPane.add(txtEmail);
			txtEmail.setColumns(10);
		}
		
		{
			btnLast = new JButton(">|");
			btnLast.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					do_btnLast_actionPerformed(e);
				}
			});
			btnLast.setBounds(604, 133, 70, 23);
			contentPane.add(btnLast);
		}
		{
			btnFirst = new JButton("|<");
			btnFirst.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					do_btnFirst_actionPerformed(e);
				}
			});
			btnFirst.setBounds(10, 133, 70, 23);
			contentPane.add(btnFirst);
		}
		{
			btnBackwards = new JButton("<<");
			btnBackwards.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					do_btnBackwards_actionPerformed(e);
				}
			});
			btnBackwards.setBounds(90, 133, 89, 23);
			contentPane.add(btnBackwards);
		}
		{
			btnForward = new JButton(">>");
			btnForward.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					do_btnForward_actionPerformed(e);
				}
			});
			btnForward.setBounds(505, 133, 89, 23);
			contentPane.add(btnForward);
		}
		{
			btnDelete = new JButton("L\u00F6schen");
			btnDelete.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					do_btnDelete_actionPerformed(e);
				}
			});
			btnDelete.setBounds(406, 133, 89, 23);
			contentPane.add(btnDelete);
		}
		{
			btnSave = new JButton("Speichern");
			btnSave.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					do_btnSave_actionPerformed(e);
				}
			});
			btnSave.setBounds(288, 133, 108, 23);
			contentPane.add(btnSave);
		}
		{
			btnNew = new JButton("Neu");
			btnNew.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					do_btnNew_actionPerformed(e);
				}
			});
			btnNew.setBounds(189, 133, 89, 23);
			contentPane.add(btnNew);
		}
	}
	
	protected void do_btnFirst_actionPerformed(ActionEvent e) {
		
		EmailKontakt eK = this.emailKontaktDAO.first();
		
		this.setTextFields(eK);
		
	}
	
	protected void do_btnLast_actionPerformed(ActionEvent e) {
		
		EmailKontakt eK = this.emailKontaktDAO.last();
		
		this.setTextFields(eK);
		
	}
	
	protected void do_btnSearch_actionPerformed(ActionEvent e) {
		
		int id = 0;
		
		try {
			id = Integer.parseInt(this.txtId.getText());
			
		} catch (Exception eID) {
			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog(this, "Bitte eine Zahl als ID angeben", "Fehler", JOptionPane.WARNING_MESSAGE);
			return;
			
		}
					
		EmailKontakt eK = this.emailKontaktDAO.select(id);
		this.setTextFields(eK);
		
	}
	
	protected void do_btnBackwards_actionPerformed(ActionEvent e) {
		
		if (lastEK.getId() == -1) {
			this.show404JOptionPane();
			return;
		}
		
		EmailKontakt eK = this.emailKontaktDAO.previous(lastEK);
		this.setTextFields(eK);
		
	}
	
	protected void do_btnForward_actionPerformed(ActionEvent e) {
		
		EmailKontakt eK = this.emailKontaktDAO.next(lastEK);
		
		this.setTextFields(eK);
		
	}
	
	protected void do_btnDelete_actionPerformed(ActionEvent e) {
		
		if (lastEK.getId() < 1) {
			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog(this, "Bitte erst Kontakt �ber Suchen ausw�hlen", "Fehler", JOptionPane.ERROR_MESSAGE);
		}
		
		this.emailKontaktDAO.delete(lastEK);
		this.clearTextFields();
		Toolkit.getDefaultToolkit().beep();
		JOptionPane.showMessageDialog(this, "Eintrag erfolgreich gel�scht", "Gel�scht", JOptionPane.INFORMATION_MESSAGE);
		
	}
	
	protected void do_btnNew_actionPerformed(ActionEvent e) {
		
		this.clearTextFields();
		
	}
	
	protected void do_btnSave_actionPerformed(ActionEvent e) {
		
		EmailKontakt eK = new EmailKontakt(0, this.txtVorname.getText(), this.txtNachname.getText(), this.txtEmail.getText());
		this.setTextFields(this.emailKontaktDAO.create(eK));
		
	}
	
	private void setTextFields(EmailKontakt eK) {
		
		if (eK.getId() == -1) {
			this.show404JOptionPane();
			return;
		}
		
		this.txtId.setText(eK.getId() + "");
		this.txtVorname.setText(eK.getVorname());
		this.txtNachname.setText(eK.getNachname());
		this.txtEmail.setText(eK.getEmail());
		
		lastEK = eK;
		
	}
	
	private void clearTextFields() {
		
		this.txtId.setText("");
		this.txtVorname.setText("");
		this.txtNachname.setText("");
		this.txtEmail.setText("");
		
	}
	
	private void show404JOptionPane() {
		Toolkit.getDefaultToolkit().beep();
		JOptionPane.showMessageDialog(this, "Keine Daten zur ID gefunden", "Fehler - Nichts gefunden", JOptionPane.ERROR_MESSAGE);
	}
	
}
